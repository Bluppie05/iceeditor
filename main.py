from flask import Flask, render_template, request, send_from_directory, jsonify
import os
import urllib.parse
import json
from multiprocessing import Process
import platform

app = Flask(__name__.split('.')[0], static_url_path="")

@app.route('/')
def index():
    f = request.args.get("f")
    return(render_template("index.html", file=f))

@app.route('/editor')
def editor():
    f = request.args.get("f")
    print(f)
    if(f != None and f != "None" and f != ""):
        try:
            readfile = open(f, "r")
            f = readfile.read()
        except:
            f = ""
    else:
        f = "None"

    fencode = urllib.parse.quote(f)
    return(render_template("editor.html", file=fencode))

@app.route('/files')
def files():
    path = request.args.get("p")
    obj = os.scandir(path)
    print(obj)
    return(render_template("files.html", folder=obj, path=path))

@app.route('/selectdir')
def selectdir():
    path = request.args.get("p")
    obj = os.scandir(path)
    print(obj)
    return(render_template("selectdir.html", folder=obj, path=path))

@app.route('/save', methods=['POST'])
def save():
    rawjson = request.get_json()
    obj = rawjson
    print(obj)
    try:
        fname = obj["file"]
        fcontent = obj["content"]
        file = open(fname,"w")
        file.write(fcontent)
        file.close()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except:
        return json.dumps({'success':False}), 200, {'ContentType':'application/json'} 

# serve static path js
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

# serve static path ace
@app.route('/ace/<path:path>')
def send_ace(path):
    return send_from_directory('ace', path)

# serve static path css
@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('css', path)

# serve static path img
@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('img', path)

# serve static path img
@app.route('/cdn/<path:path>')
def send_cdn(path):
    return send_from_directory('cdn', path)

if(platform.system() == "Linux"):
    os.system("browser/linux/chrome --app='http://127.0.0.1:5000/' &")
elif(platform.system() == "Windows"):
    os.system("start browser/win32/chrome.exe --app='http://127.0.0.1:5000/'")