(function(){'use strict';window.viewportSize={width:800,height:600};window.deviceScaleFactor=1;window.emulationScaleFactor=1;window.pageScaleFactor=1;window.pageZoomFactor=1;window.scrollX=0;window.scrollY=0;function reset(resetData){window.viewportSize=resetData.viewportSize;window.deviceScaleFactor=resetData.deviceScaleFactor;window.pageScaleFactor=resetData.pageScaleFactor;window.pageZoomFactor=resetData.pageZoomFactor;window.emulationScaleFactor=resetData.emulationScaleFactor;window.scrollX=Math.round(resetData.scrollX);window.scrollY=Math.round(resetData.scrollY);window.canvas=document.getElementById('canvas');if(window.canvas){window.canvas.width=deviceScaleFactor*viewportSize.width;window.canvas.height=deviceScaleFactor*viewportSize.height;window.canvas.style.width=viewportSize.width+'px';window.canvas.style.height=viewportSize.height+'px';window.context=canvas.getContext('2d');window.context.scale(deviceScaleFactor,deviceScaleFactor);window.canvasWidth=viewportSize.width;window.canvasHeight=viewportSize.height;}}
function setPlatform(platform){window.platform=platform;document.body.classList.add('platform-'+platform);}
function dispatch(message){const functionName=message.shift();window[functionName].apply(null,message);}
Element.prototype.createChild=function(tagName,className){const element=createElement(tagName,className);element.addEventListener('click',function(e){e.stopPropagation();},false);this.appendChild(element);return element;};Element.prototype.createTextChild=function(text){const element=document.createTextNode(text);this.appendChild(element);return element;};Element.prototype.removeChildren=function(){if(this.firstChild){this.textContent='';}};function createElement(tagName,className){const element=document.createElement(tagName);if(className){element.className=className;}
return element;}
String.prototype.trimEnd=function(maxLength){if(this.length<=maxLength){return String(this);}
return this.substr(0,maxLength-1)+'\u2026';};Number.constrain=function(num,min,max){if(num<min){num=min;}else if(num>max){num=max;}
return num;};const GridArrowTypes={leftTop:'left-top',leftMid:'left-mid',leftBottom:'left-bottom',topLeft:'top-left',topMid:'top-mid',topRight:'top-right',rightTop:'right-top',rightMid:'right-mid',rightBottom:'right-bottom',bottomLeft:'bottom-left',bottomMid:'bottom-mid',bottomRight:'bottom-right',};const gridArrowWidth=3;const gridPageMargin=20;const gridLabelDistance=20;function drawGridLabels(config,gridBounds,areaBounds){const labelContainerId=window._gridLayerCounter?`grid-${window._gridLayerCounter++}-labels`:'grid-labels';let labelContainerForNode=document.getElementById(labelContainerId);if(!labelContainerForNode){const mainLabelLayerContainer=document.getElementById('grid-label-container');labelContainerForNode=mainLabelLayerContainer.createChild('div');labelContainerForNode.id=labelContainerId;}
labelContainerForNode.removeChildren();const lineNumberContainer=labelContainerForNode.createChild('div','line-numbers');const areaNameContainer=labelContainerForNode.createChild('div','area-names');const trackSizesContainer=labelContainerForNode.createChild('div','track-sizes');drawGridNumbers(lineNumberContainer,config,gridBounds);drawGridAreaNames(areaNameContainer,areaBounds);if(config.columnTrackSizes){drawGridTrackSizes(trackSizesContainer,config.rotationAngle,config.columnTrackSizes,'column');}
if(config.rowTrackSizes){drawGridTrackSizes(trackSizesContainer,config.rotationAngle,config.rowTrackSizes,'row');}}
function*offsetIterator(offsets){let lastEmittedOffset=null;for(const[i,offset]of offsets.entries()){const isFirst=i===0;const isLast=i===offsets.length-1;const isFarEnoughFromPrevious=offset-lastEmittedOffset>gridLabelDistance;const isFarEnoughFromLast=!isLast&&offsets[offsets.length-1]-offset>gridLabelDistance;if(isFirst||isLast||(isFarEnoughFromPrevious&&isFarEnoughFromLast)){yield[i,offset];lastEmittedOffset=offset;}}}
function _normalizeOffsetData(config,bounds){const width=Math.round(bounds.maxX-bounds.minX);const height=Math.round(bounds.maxY-bounds.minY);const data={rows:{positive:{offsets:[],hasFirst:false,hasLast:false},negative:{offsets:[],hasFirst:false,hasLast:false},},columns:{positive:{offsets:[],hasFirst:false,hasLast:false},negative:{offsets:[],hasFirst:false,hasLast:false},},bounds:{minX:Math.round(bounds.minX),maxX:Math.round(bounds.maxX),minY:Math.round(bounds.minY),maxY:Math.round(bounds.maxY),width,height,}};if(config.positiveRowLineNumberOffsets){data.rows.positive={offsets:config.positiveRowLineNumberOffsets.map(offset=>Math.round(offset)),hasFirst:Math.round(config.positiveRowLineNumberOffsets[0])===0,hasLast:Math.round(config.positiveRowLineNumberOffsets[config.positiveRowLineNumberOffsets.length-1])===height};}
if(config.negativeRowLineNumberOffsets){data.rows.negative={offsets:config.negativeRowLineNumberOffsets.map(offset=>Math.round(offset)),hasFirst:Math.round(config.negativeRowLineNumberOffsets[0])===0,hasLast:Math.round(config.negativeRowLineNumberOffsets[config.negativeRowLineNumberOffsets.length-1])===height};}
if(config.positiveColumnLineNumberOffsets){data.columns.positive={offsets:config.positiveColumnLineNumberOffsets.map(offset=>Math.round(offset)),hasFirst:Math.round(config.positiveColumnLineNumberOffsets[0])===0,hasLast:Math.round(config.positiveColumnLineNumberOffsets[config.positiveColumnLineNumberOffsets.length-1])===width};}
if(config.negativeColumnLineNumberOffsets){data.columns.negative={offsets:config.negativeColumnLineNumberOffsets.map(offset=>Math.round(offset)),hasFirst:Math.round(config.negativeColumnLineNumberOffsets[0])===0,hasLast:Math.round(config.negativeColumnLineNumberOffsets[config.negativeColumnLineNumberOffsets.length-1])===width};}
return data;}
function drawGridNumbers(container,config,bounds){const data=_normalizeOffsetData(config,bounds);for(const[i,offset]of offsetIterator(data.columns.positive.offsets)){const element=_createLabelElement(container,i+1);_placePositiveColumnLabel(element,offset,data);}
for(const[i,offset]of offsetIterator(data.rows.positive.offsets)){const element=_createLabelElement(container,i+1);_placePositiveRowLabel(element,offset,data);}
for(const[i,offset]of offsetIterator(data.columns.negative.offsets)){const element=_createLabelElement(container,data.columns.negative.offsets.length*-1+i);_placeNegativeColumnLabel(element,offset,data);}
for(const[i,offset]of offsetIterator(data.rows.negative.offsets)){const element=_createLabelElement(container,data.rows.negative.offsets.length*-1+i);_placeNegativeRowLabel(element,offset,data);}}
function drawGridTrackSizes(container,rotationAngle,trackSizes,direction){for(const{x,y,computedSize}of trackSizes){const size=computedSize.toFixed(2);const element=_createLabelElement(container,(size.endsWith('.00')?size.slice(0,-3):size)+'px');const labelWidth=_getAdjustedLabelWidth(element);const labelHeight=element.getBoundingClientRect().height;const arrowType=direction==='column'?GridArrowTypes.bottomMid:GridArrowTypes.rightMid;const{contentLeft,contentTop}=_getLabelPositionByArrowType(arrowType,x,y,labelWidth,labelHeight);element.classList.add(arrowType);element.style.left=contentLeft+'px';element.style.top=contentTop+'px';}}
function drawGridAreaNames(container,areaBounds){for(const{name,bounds}of areaBounds){const element=_createLabelElement(container,name);element.style.left=bounds.minX+'px';element.style.top=bounds.minY+'px';}}
function _createLabelElement(container,textContent){const wrapper=container.createChild('div');const element=wrapper.createChild('div','grid-label-content');element.textContent=textContent.toString();return element;}
function _placePositiveRowLabel(element,offset,data){const x=data.bounds.minX;const y=data.bounds.minY+offset;const isAtSharedStartCorner=offset===0&&data.columns&&data.columns.positive.hasFirst;const isAtSharedEndCorner=offset===data.bounds.height&&data.columns&&data.columns.negative.hasFirst;const isTooCloseToViewportStart=y<gridPageMargin;const isTooCloseToViewportEnd=canvasHeight-y<gridPageMargin;const flipIn=x<gridPageMargin;if(flipIn&&(isAtSharedStartCorner||isAtSharedEndCorner)){element.classList.add('inner-shared-corner');}
let arrowType=GridArrowTypes.rightMid;if(isTooCloseToViewportStart||isAtSharedStartCorner){arrowType=GridArrowTypes.rightTop;}else if(isTooCloseToViewportEnd||isAtSharedEndCorner){arrowType=GridArrowTypes.rightBottom;}
arrowType=_flipArrowTypeIfNeeded(arrowType,flipIn);_placeLineNumberLabel(element,arrowType,x,y);}
function _placeNegativeRowLabel(element,offset,data){const x=data.bounds.maxX;const y=data.bounds.minY+offset;const isAtSharedStartCorner=offset===0&&data.columns&&data.columns.positive.hasLast;const isAtSharedEndCorner=offset===data.bounds.height&&data.columns&&data.columns.negative.hasLast;const isTooCloseToViewportStart=y<gridPageMargin;const isTooCloseToViewportEnd=canvasHeight-y<gridPageMargin;const flipIn=canvasWidth-x<gridPageMargin;if(flipIn&&(isAtSharedStartCorner||isAtSharedEndCorner)){element.classList.add('inner-shared-corner');}
let arrowType=GridArrowTypes.leftMid;if(isTooCloseToViewportStart||isAtSharedStartCorner){arrowType=GridArrowTypes.leftTop;}else if(isTooCloseToViewportEnd||isAtSharedEndCorner){arrowType=GridArrowTypes.leftBottom;}
arrowType=_flipArrowTypeIfNeeded(arrowType,flipIn);_placeLineNumberLabel(element,arrowType,x,y);}
function _placePositiveColumnLabel(element,offset,data){const x=data.bounds.minX+offset;const y=data.bounds.minY;const isAtSharedStartCorner=offset===0&&data.rows&&data.rows.positive.hasFirst;const isAtSharedEndCorner=offset===data.bounds.width&&data.rows&&data.rows.negative.hasFirst;const isTooCloseToViewportStart=x<gridPageMargin;const isTooCloseToViewportEnd=canvasWidth-x<gridPageMargin;const flipIn=y<gridPageMargin;if(flipIn&&(isAtSharedStartCorner||isAtSharedEndCorner)){element.classList.add('inner-shared-corner');}
let arrowType=GridArrowTypes.bottomMid;if(isTooCloseToViewportStart){arrowType=GridArrowTypes.bottomLeft;}else if(isTooCloseToViewportEnd){arrowType=GridArrowTypes.bottomRight;}
arrowType=_flipArrowTypeIfNeeded(arrowType,flipIn);_placeLineNumberLabel(element,arrowType,x,y);}
function _placeNegativeColumnLabel(element,offset,data){const x=data.bounds.minX+offset;const y=data.bounds.maxY;const isAtSharedStartCorner=offset===0&&data.rows&&data.rows.positive.hasLast;const isAtSharedEndCorner=offset===data.bounds.width&&data.rows&&data.rows.negative.hasLast;const isTooCloseToViewportStart=x<gridPageMargin;const isTooCloseToViewportEnd=canvasWidth-x<gridPageMargin;const flipIn=canvasHeight-y<gridPageMargin;if(flipIn&&(isAtSharedStartCorner||isAtSharedEndCorner)){element.classList.add('inner-shared-corner');}
let arrowType=GridArrowTypes.topMid;if(isTooCloseToViewportStart){arrowType=GridArrowTypes.topLeft;}else if(isTooCloseToViewportEnd){arrowType=GridArrowTypes.topRight;}
arrowType=_flipArrowTypeIfNeeded(arrowType,flipIn);_placeLineNumberLabel(element,arrowType,x,y);}
function _placeLineNumberLabel(element,arrowType,x,y){const labelWidth=_getAdjustedLabelWidth(element);const labelHeight=element.getBoundingClientRect().height;const{contentLeft,contentTop}=_getLabelPositionByArrowType(arrowType,x,y,labelWidth,labelHeight);element.classList.add(arrowType);element.style.left=contentLeft+'px';element.style.top=contentTop+'px';}
function _getAdjustedLabelWidth(element){let labelWidth=element.getBoundingClientRect().width;if(labelWidth%2===1){labelWidth+=1;element.style.width=labelWidth+'px';}
return labelWidth;}
function _flipArrowTypeIfNeeded(arrowType,flipIn){if(!flipIn){return arrowType;}
switch(arrowType){case GridArrowTypes.leftTop:return GridArrowTypes.rightTop;case GridArrowTypes.leftMid:return GridArrowTypes.rightMid;case GridArrowTypes.leftBottom:return GridArrowTypes.rightBottom;case GridArrowTypes.rightTop:return GridArrowTypes.leftTop;case GridArrowTypes.rightMid:return GridArrowTypes.leftMid;case GridArrowTypes.rightBottom:return GridArrowTypes.leftBottom;case GridArrowTypes.topLeft:return GridArrowTypes.bottomLeft;case GridArrowTypes.topMid:return GridArrowTypes.bottomMid;case GridArrowTypes.topRight:return GridArrowTypes.bottomRight;case GridArrowTypes.bottomLeft:return GridArrowTypes.topLeft;case GridArrowTypes.bottomMid:return GridArrowTypes.topMid;case GridArrowTypes.bottomRight:return GridArrowTypes.topRight;}}
function _getLabelPositionByArrowType(arrowType,x,y,labelWidth,labelHeight){let contentTop;let contentLeft;switch(arrowType){case GridArrowTypes.leftTop:contentTop=y;contentLeft=x+gridArrowWidth;break;case GridArrowTypes.leftMid:contentTop=y-(labelHeight/2);contentLeft=x+gridArrowWidth;break;case GridArrowTypes.leftBottom:contentTop=y-labelHeight;contentLeft=x+gridArrowWidth;break;case GridArrowTypes.rightTop:contentTop=y;contentLeft=x-gridArrowWidth-labelWidth;break;case GridArrowTypes.rightMid:contentTop=y-(labelHeight/2);contentLeft=x-gridArrowWidth-labelWidth;break;case GridArrowTypes.rightBottom:contentTop=y-labelHeight;contentLeft=x-labelWidth-gridArrowWidth;break;case GridArrowTypes.topLeft:contentTop=y+gridArrowWidth;contentLeft=x;break;case GridArrowTypes.topMid:contentTop=y+gridArrowWidth;contentLeft=x-(labelWidth/2);break;case GridArrowTypes.topRight:contentTop=y+gridArrowWidth;contentLeft=x-labelWidth;break;case GridArrowTypes.bottomLeft:contentTop=y-gridArrowWidth-labelHeight;contentLeft=x;break;case GridArrowTypes.bottomMid:contentTop=y-gridArrowWidth-labelHeight;contentLeft=x-(labelWidth/2);break;case GridArrowTypes.bottomRight:contentTop=y-gridArrowWidth-labelHeight;contentLeft=x-labelWidth;break;}
return{contentTop,contentLeft,};}
function drawRulers(context,bounds,rulerAtRight,rulerAtBottom,color,dash){context.save();const width=canvasWidth;const height=canvasHeight;context.strokeStyle=color||'rgba(128, 128, 128, 0.3)';context.lineWidth=1;context.translate(0.5,0.5);if(dash){context.setLineDash([3,3]);}
if(rulerAtRight){for(const y in bounds.rightmostXForY){context.beginPath();context.moveTo(width,y);context.lineTo(bounds.rightmostXForY[y],y);context.stroke();}}else{for(const y in bounds.leftmostXForY){context.beginPath();context.moveTo(0,y);context.lineTo(bounds.leftmostXForY[y],y);context.stroke();}}
if(rulerAtBottom){for(const x in bounds.bottommostYForX){context.beginPath();context.moveTo(x,height);context.lineTo(x,bounds.topmostYForX[x]);context.stroke();}}else{for(const x in bounds.topmostYForX){context.beginPath();context.moveTo(x,0);context.lineTo(x,bounds.topmostYForX[x]);context.stroke();}}
context.restore();}
function buildPath(commands,bounds){let commandsIndex=0;function extractPoints(count){const points=[];for(let i=0;i<count;++i){const x=Math.round(commands[commandsIndex++]*emulationScaleFactor);bounds.maxX=Math.max(bounds.maxX,x);bounds.minX=Math.min(bounds.minX,x);const y=Math.round(commands[commandsIndex++]*emulationScaleFactor);bounds.maxY=Math.max(bounds.maxY,y);bounds.minY=Math.min(bounds.minY,y);bounds.leftmostXForY[y]=Math.min(bounds.leftmostXForY[y]||Number.MAX_VALUE,x);bounds.rightmostXForY[y]=Math.max(bounds.rightmostXForY[y]||Number.MIN_VALUE,x);bounds.topmostYForX[x]=Math.min(bounds.topmostYForX[x]||Number.MAX_VALUE,y);bounds.bottommostYForX[x]=Math.max(bounds.bottommostYForX[x]||Number.MIN_VALUE,y);points.push(x,y);}
return points;}
const commandsLength=commands.length;const path=new Path2D();while(commandsIndex<commandsLength){switch(commands[commandsIndex++]){case'M':path.moveTo.apply(path,extractPoints(1));break;case'L':path.lineTo.apply(path,extractPoints(1));break;case'C':path.bezierCurveTo.apply(path,extractPoints(3));break;case'Q':path.quadraticCurveTo.apply(path,extractPoints(2));break;case'Z':path.closePath();break;}}
return path;}
function emptyBounds(){const bounds={minX:Number.MAX_VALUE,minY:Number.MAX_VALUE,maxX:Number.MIN_VALUE,maxY:Number.MIN_VALUE,leftmostXForY:{},rightmostXForY:{},topmostYForX:{},bottommostYForX:{}};return bounds;}
const gridStyle=`
/* Grid row and column labels */
.grid-label-content {
  position: absolute;
  z-index: 10;
  -webkit-user-select: none;
}

.grid-label-content {
  background-color: #1A73E8;
  padding: 2px;
  font-family: Menlo, monospace;
  font-size: 10px;
  min-width: 17px;
  min-height: 15px;
  color: #FFFFFF;
  border-radius: 2px;
  box-sizing: border-box;
  z-index: 1;
  background-clip: padding-box;
  pointer-events: none;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
}

.track-sizes .grid-label-content,
.line-numbers .grid-label-content {
  border: 1px solid white;
  --inner-corner-avoid-distance: 15px;
}

.grid-label-content.top-left.inner-shared-corner,
.grid-label-content.top-right.inner-shared-corner {
  transform: translateY(var(--inner-corner-avoid-distance));
}

.grid-label-content.bottom-left.inner-shared-corner,
.grid-label-content.bottom-right.inner-shared-corner {
  transform: translateY(calc(var(--inner-corner-avoid-distance) * -1));
}

.grid-label-content.left-top.inner-shared-corner,
.grid-label-content.left-bottom.inner-shared-corner {
  transform: translateX(var(--inner-corner-avoid-distance));
}

.grid-label-content.right-top.inner-shared-corner,
.grid-label-content.right-bottom.inner-shared-corner {
  transform: translateX(calc(var(--inner-corner-avoid-distance) * -1));
}

.track-sizes .grid-label-content::before,
.line-numbers .grid-label-content::before {
  position: absolute;
  z-index: 1;
  pointer-events: none;
  content: "";
  background: #1A73E8;
  width: 3px;
  height: 3px;
  border: 1px solid white;
  border-width: 0 1px 1px 0;
}

.grid-label-content.bottom-mid::before {
  transform: translateY(-1px) rotate(45deg);
  top: 100%;
}

.grid-label-content.top-mid::before {
  transform: translateY(-3px) rotate(-135deg);
  top: 0%;
}

.grid-label-content.left-mid::before {
  transform: translateX(-3px) rotate(135deg);
  left: 0%
}

.grid-label-content.right-mid::before {
  transform: translateX(3px) rotate(-45deg);
  right: 0%;
}

.grid-label-content.right-top::before {
  transform: translateX(3px) translateY(-1px) rotate(-90deg) skewY(30deg);
  right: 0%;
  top: 0%;
}

.grid-label-content.right-bottom::before {
  transform: translateX(3px) translateY(-3px) skewX(30deg);
  right: 0%;
  top: 100%;
}

.grid-label-content.bottom-right::before {
  transform:  translateX(1px) translateY(-1px) skewY(30deg);
  right: 0%;
  top: 100%;
}

.grid-label-content.bottom-left::before {
  transform:  translateX(-1px) translateY(-1px) rotate(90deg) skewX(30deg);
  left: 0%;
  top: 100%;
}

.grid-label-content.left-top::before {
  transform: translateX(-3px) translateY(-1px) rotate(180deg) skewX(30deg);
  left: 0%;
  top: 0%;
}

.grid-label-content.left-bottom::before {
  transform: translateX(-3px) translateY(-3px) rotate(90deg) skewY(30deg);
  left: 0%;
  top: 100%;
}

.grid-label-content.top-right::before {
  transform:  translateX(1px) translateY(-3px) rotate(-90deg) skewX(30deg);
  right: 0%;
  top: 0%;
}

.grid-label-content.top-left::before {
  transform:  translateX(-1px) translateY(-3px) rotate(180deg) skewY(30deg);
  left: 0%;
  top: 0%;
}

@media (forced-colors: active) {
  .grid-label-content {
      border-color: Highlight;
      background-color: Canvas;
      color: Text;
      forced-color-adjust: none;
  }
  .grid-label-content::before {
    background-color: Canvas;
    border-color: Highlight;
  }
}`;function drawLayoutGridHighlight(highlight,context){const gridBounds=emptyBounds();const gridPath=buildPath(highlight.gridBorder,gridBounds);if(highlight.gridHighlightConfig.gridBorderColor){context.save();context.translate(0.5,0.5);context.lineWidth=0;if(highlight.gridHighlightConfig.gridBorderDash){context.setLineDash([3,3]);}
context.strokeStyle=highlight.gridHighlightConfig.gridBorderColor;context.stroke(gridPath);context.restore();}
if(highlight.gridHighlightConfig.cellBorderColor){const rowBounds=emptyBounds();const columnBounds=emptyBounds();const rowPath=buildPath(highlight.rows,rowBounds);const columnPath=buildPath(highlight.columns,columnBounds);context.save();context.translate(0.5,0.5);if(highlight.gridHighlightConfig.cellBorderDash){context.setLineDash([3,3]);}
context.lineWidth=0;context.strokeStyle=highlight.gridHighlightConfig.cellBorderColor;context.save();context.stroke(rowPath);context.restore();context.save();context.stroke(columnPath);context.restore();context.restore();if(highlight.gridHighlightConfig.showGridExtensionLines){drawRulers(context,rowBounds,false,false,highlight.gridHighlightConfig.cellBorderColor,highlight.gridHighlightConfig.cellBorderDash);drawRulers(context,rowBounds,true,true,highlight.gridHighlightConfig.cellBorderColor,highlight.gridHighlightConfig.cellBorderDash);drawRulers(context,columnBounds,false,false,highlight.gridHighlightConfig.cellBorderColor,highlight.gridHighlightConfig.cellBorderDash);drawRulers(context,columnBounds,true,true,highlight.gridHighlightConfig.cellBorderColor,highlight.gridHighlightConfig.cellBorderDash);}}
_drawGridGap(context,highlight.rowGaps,highlight.gridHighlightConfig.rowGapColor,highlight.gridHighlightConfig.rowHatchColor,highlight.rotationAngle,true);_drawGridGap(context,highlight.columnGaps,highlight.gridHighlightConfig.columnGapColor,highlight.gridHighlightConfig.columnHatchColor,highlight.rotationAngle);const areaBounds=_drawGridAreas(context,highlight.areaNames,highlight.gridHighlightConfig.areaBorderColor);drawGridLabels(highlight,gridBounds,areaBounds);}
function _drawGridAreas(context,areas,borderColor){if(!areas||!Object.keys(areas).length){return[];}
context.save();if(borderColor){context.strokeStyle=borderColor;}
context.lineWidth=2;const areaBounds=[];for(const name in areas){const areaCommands=areas[name];const bounds=emptyBounds();const path=buildPath(areaCommands,bounds);context.stroke(path);areaBounds.push({name,bounds});}
context.restore();return areaBounds;}
function _drawGridGap(context,gapCommands,gapColor,hatchColor,rotationAngle,flipDirection){if(!gapColor&&!hatchColor){return;}
context.save();context.translate(0.5,0.5);context.lineWidth=0;const bounds=emptyBounds();const path=buildPath(gapCommands,bounds);if(gapColor){context.fillStyle=gapColor;context.fill(path);}
if(hatchColor){_hatchFillPath(context,path,bounds,10,hatchColor,rotationAngle,flipDirection);}
context.restore();}
function _hatchFillPath(context,path,bounds,delta,color,rotationAngle,flipDirection){const dx=bounds.maxX-bounds.minX;const dy=bounds.maxY-bounds.minY;context.rect(bounds.minX,bounds.minY,dx,dy);context.save();context.clip(path);context.setLineDash([5,3]);const majorAxis=Math.max(dx,dy);context.strokeStyle=color;const centerX=bounds.minX+dx/2;const centerY=bounds.minY+dy/2;context.translate(centerX,centerY);context.rotate(rotationAngle*Math.PI/180);context.translate(-centerX,-centerY);if(flipDirection){for(let i=-majorAxis;i<majorAxis;i+=delta){context.beginPath();context.moveTo(bounds.maxX-i,bounds.minY);context.lineTo(bounds.maxX-dy-i,bounds.maxY);context.stroke();}}else{for(let i=-majorAxis;i<majorAxis;i+=delta){context.beginPath();context.moveTo(i+bounds.minX,bounds.minY);context.lineTo(dy+i+bounds.minX,bounds.maxY);context.stroke();}}
context.restore();}
function doReset(){document.getElementById('grid-label-container').removeChildren();window._gridLayerCounter=1;window._gridPainted=false;}
function drawGridHighlight(highlight,context){context=context||window.context;context.save();drawLayoutGridHighlight(highlight,context);context.restore();return;}
const style=`
@media (forced-colors: active) {
  :root, body {
      background-color: transparent;
      forced-color-adjust: none;
  }
}`;window.setPlatform=function(platform){const styleTag=document.createElement('style');styleTag.textContent=`${style}${gridStyle}`;document.head.append(styleTag);document.body.classList.add('fill');const canvas=document.createElement('canvas');canvas.id='canvas';canvas.classList.add('fill');document.body.append(canvas);const gridLabels=document.createElement('div');gridLabels.id='grid-label-container';document.body.append(gridLabels);setPlatform(platform);};window.reset=function(data){reset(data);doReset();};window.drawGridHighlight=drawGridHighlight;window.dispatch=dispatch;}());