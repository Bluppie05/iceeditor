function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function editor_get() {
    var editor = document.getElementById("editorbox")
    var output = editor.contentWindow.document.getElementById("output").innerHTML;
    return(output);
}

function save() {
    var fcontent = editor_get();
    var fname = getUrlVars()["f"];
    var dict = {content : fcontent , file : fname};

    $.ajax({
        type: "POST", 
        url: "http://127.0.0.1:5000/save", //localhost Flask
        data : JSON.stringify(dict),
        contentType: "application/json",
    });
}

function changeLang() {
    document.getElementById('editorbox').contentWindow.postMessage('changelang', '*');
}

function openfile() {
    document.getElementById("popup").style.display = "block";
}

function newfile() {
    document.getElementById("popup2").style.display = "block";
}

function newfileproc() {
    var dir = document.getElementById("dirbox").contentWindow.location.href;
    var fname = document.getElementById("filename").value;
    var dir = dir.replace("selectdir?p=", "?f=");
    window.location.href = dir+"/"+fname;
}